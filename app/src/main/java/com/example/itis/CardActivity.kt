package com.example.itis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_card.*

import com.example.itis.CardsViewHolder.Companion.HERO_ID
import com.example.itis.CardsViewHolder.Companion.HERO_IMAGE_URL
import com.example.itis.CardsViewHolder.Companion.HERO_NAME
import com.example.itis.CardsViewHolder.Companion.HERO_ALIGNMENT
import com.example.itis.CardsViewHolder.Companion.HERO_ALIGNMENT_BAD
import com.example.itis.CardsViewHolder.Companion.HERO_ALIGNMENT_GOOD
import com.example.itis.CardsViewHolder.Companion.HERO_INTELLIGENCE
import com.example.itis.CardsViewHolder.Companion.HERO_STRENGTH
import com.example.itis.CardsViewHolder.Companion.HERO_SPEED
import com.example.itis.CardsViewHolder.Companion.HERO_DURABILITY
import com.example.itis.CardsViewHolder.Companion.HERO_COMBAT
import com.google.firebase.analytics.FirebaseAnalytics


class CardActivity : AppCompatActivity() {

    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        val ID = intent.getIntExtra(HERO_ID, 0)
        val NAME = intent.getStringExtra(HERO_NAME)
        val IMAGE_URL = intent.getStringExtra(HERO_IMAGE_URL)
        val ALIGNMENT = intent.getStringExtra(HERO_ALIGNMENT)
        val INTELLIGENCE = intent.getIntExtra(HERO_INTELLIGENCE, 0)
        val STRENGTH = intent.getIntExtra(HERO_STRENGTH, 0)
        val SPEED = intent.getIntExtra(HERO_SPEED, 0)
        val DURABILITY = intent.getIntExtra(HERO_DURABILITY, 0)
        val COMBAT = intent.getIntExtra(HERO_COMBAT, 0)



        Picasso.with(this).load(IMAGE_URL).into(picture)

        hero_id.text = ID.toString()
        hero_name.text = NAME

        when (ALIGNMENT) {
            HERO_ALIGNMENT_GOOD -> Picasso.with(this).load(R.drawable.good).into(hero_alignment)
            HERO_ALIGNMENT_BAD -> Picasso.with(this).load(R.drawable.bad).into(hero_alignment)
        }

        hero_intelligence.text = getString(R.string.intelligence, INTELLIGENCE)
        hero_strength.text = getString(R.string.strength, STRENGTH)
        hero_speed.text = getString(R.string.speed, SPEED)
        hero_durability.text = getString(R.string.durability, DURABILITY)
        hero_combat.text = getString(R.string.combat, COMBAT)


        val fightButton: Button = findViewById(R.id.fight_button)

        fightButton.setOnClickListener {

            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "fight_button")
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "fight_button")
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "fight_button_click")
            mFirebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

            val intent = Intent(this@CardActivity, MainActivity::class.java)

            intent.putExtra(HERO_NAME, NAME)
            intent.putExtra(HERO_INTELLIGENCE, INTELLIGENCE)
            intent.putExtra(HERO_STRENGTH, STRENGTH)
            intent.putExtra(HERO_SPEED, SPEED)
            intent.putExtra(HERO_DURABILITY, DURABILITY)
            intent.putExtra(HERO_COMBAT, COMBAT)

            this@CardActivity.startActivity(intent)
        }
    }
}