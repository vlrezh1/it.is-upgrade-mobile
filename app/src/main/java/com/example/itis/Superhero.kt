package com.example.itis

import android.content.Intent
import com.example.itis.CardsViewHolder.Companion.HERO_NAME
import com.example.itis.CardsViewHolder.Companion.HERO_INTELLIGENCE
import com.example.itis.CardsViewHolder.Companion.HERO_STRENGTH
import com.example.itis.CardsViewHolder.Companion.HERO_SPEED
import com.example.itis.CardsViewHolder.Companion.HERO_DURABILITY
import com.example.itis.CardsViewHolder.Companion.HERO_COMBAT


data class Images(
    val xs: String = "",
    val lg: String = ""
)

data class Biography(
    val alignment: String = ""
)

data class Powerstats(
    val intelligence: Int = 0,
    val strength: Int = 0,
    val speed: Int = 0,
    val durability: Int = 0,
    val combat: Int = 0
)

data class Superhero(
    val id: Int,
    val name: String,
    val biography: Biography = Biography(),
    val powerstats: Powerstats = Powerstats(),
    val images: Images = Images()
)

fun fight(first: Intent, second: Intent): String {

    val firstScore = first.getIntExtra(HERO_INTELLIGENCE, 0) +
            first.getIntExtra(HERO_STRENGTH, 0) +
            first.getIntExtra(HERO_SPEED, 0) +
            first.getIntExtra(HERO_DURABILITY, 0) +
            first.getIntExtra(HERO_COMBAT, 0)

    val secondScore = second.getIntExtra(HERO_INTELLIGENCE, 0) +
            second.getIntExtra(HERO_STRENGTH, 0) +
            second.getIntExtra(HERO_SPEED, 0) +
            second.getIntExtra(HERO_DURABILITY, 0) +
            second.getIntExtra(HERO_COMBAT, 0)

    val message: String
    val firstName = first.getStringExtra(HERO_NAME)
    val secondName = second.getStringExtra(HERO_NAME)

    message = when {
        firstScore > secondScore -> "$firstName won a fight!"
        firstScore < secondScore -> "$secondName won a fight!"
        else -> "The fight ended in a draw!"
    }

    return message
}