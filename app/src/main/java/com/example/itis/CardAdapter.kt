package com.example.itis

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_list_item.view.*
import com.squareup.picasso.Picasso


class CardAdapter(private val context: Context) : RecyclerView.Adapter<CardsViewHolder>() {

    private var superheroes: ArrayList<Superhero> = ArrayList()
    private var intent: Intent? = null

    fun setList(list: ArrayList<Superhero>) {
        this.superheroes = list
        notifyDataSetChanged()
    }

    fun setIntent(intent: Intent) {
        this.intent = intent
    }

    override fun getItemCount(): Int {
        return superheroes.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardsViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.card_list_item, parent, false)
        return CardsViewHolder(view, context)
    }

    override fun onBindViewHolder(holder: CardsViewHolder, position: Int) {
        holder.bind(superheroes[position], this.intent)
    }
}

class CardsViewHolder(private val view: View, private val context: Context) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.row_text
    private val image: ImageView = view.row_image

    fun bind(card: Superhero, newIntent: Intent?) {

        name.row_text?.text = card.name
        Picasso.with(context).load(card.images.xs).into(image)

        view.setOnClickListener {
            val intent = Intent(view.context, CardActivity::class.java)

            intent.putExtra(HERO_IMAGE_URL, card.images.lg)

            intent.putExtra(HERO_ID, card.id)
            intent.putExtra(HERO_NAME, card.name)
            intent.putExtra(HERO_ALIGNMENT, card.biography.alignment)

            intent.putExtra(HERO_INTELLIGENCE, card.powerstats.intelligence)
            intent.putExtra(HERO_STRENGTH, card.powerstats.strength)
            intent.putExtra(HERO_SPEED, card.powerstats.speed)
            intent.putExtra(HERO_DURABILITY, card.powerstats.durability)
            intent.putExtra(HERO_COMBAT, card.powerstats.combat)

            if (newIntent == null) {
                view.context.startActivity(intent)
            }
            else {
                val message = fight(newIntent, intent)

                val builder = AlertDialog.Builder(context)
                builder.setMessage(message)
                builder.setPositiveButton("OK") { _: DialogInterface, _: Int ->

                    val refresh = Intent(view.context, MainActivity::class.java)
                    refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    refresh.putExtra("EXIT", true)
                    view.context.startActivity(refresh)

                }
                builder.show()
            }
        }
    }

    companion object {
        const val HERO_ID = "id"
        const val HERO_IMAGE_URL = "image_url"
        const val HERO_NAME = "name"

        const val HERO_ALIGNMENT = "alignment"
        const val HERO_ALIGNMENT_GOOD = "good"
        const val HERO_ALIGNMENT_BAD = "bad"

        const val HERO_INTELLIGENCE = "intelligence"
        const val HERO_STRENGTH = "strength"
        const val HERO_SPEED = "speed"
        const val HERO_DURABILITY = "durability"
        const val HERO_COMBAT = "combat"
    }
}