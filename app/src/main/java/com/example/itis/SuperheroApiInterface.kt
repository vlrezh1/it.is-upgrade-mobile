package com.example.itis

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.converter.gson.GsonConverterFactory


interface SuperheroApiInterface {
    @GET("all.json")
    fun getAllHeroes() : Call<ArrayList<Superhero>>

    companion object {
        fun create(): SuperheroApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API_URL)
                .build()
            return retrofit.create(SuperheroApiInterface::class.java)
        }

        private const val API_URL = "https://akabab.github.io/superhero-api/api/"
    }
}