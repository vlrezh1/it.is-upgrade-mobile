package com.example.itis

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    private var superheroes: ArrayList<Superhero> = ArrayList()
    private var adapter: CardAdapter = CardAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cards_rv.layoutManager = LinearLayoutManager(this)
        cards_rv.adapter = adapter

        if (intent.getStringExtra(CardsViewHolder.HERO_NAME) != null) {
            Toast.makeText(this@MainActivity, "Choose your enemy!" , Toast.LENGTH_LONG).show()
            adapter.setIntent(intent)
        }

        val apiInterface = SuperheroApiInterface.create()
        apiInterface.getAllHeroes().enqueue(object : Callback<ArrayList<Superhero>> {

            override fun onResponse(call: Call<ArrayList<Superhero>>, response: Response<ArrayList<Superhero>>) {
                superheroes = response.body()!!
                adapter.setList(superheroes)
            }

            override fun onFailure(call: Call<ArrayList<Superhero>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Failed to load data." , Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val menuItem: MenuItem = menu!!.findItem(R.id.action_search)
        val searchView: SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String): Boolean {
        val userInput: String = newText.toLowerCase()
        val newList: ArrayList<Superhero> = ArrayList()

        for (hero in superheroes) {
            if (hero.name.toLowerCase().contains(userInput) || hero.id.toString().toLowerCase().contains(userInput))
                newList.add(hero)
        }
        adapter.setList(newList)
        return true
    }
}